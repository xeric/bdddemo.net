﻿using System;
namespace BDDDemo.Services
{
    public class CounterService
    {
        private int counter;

        public int Increment()
        {
            return counter++;
        }
    }
}
