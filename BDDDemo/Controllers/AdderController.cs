﻿using System;
using System.Web.Http;
using BDDDemo.Services;

namespace BDDDemo.Controllers
{
    public class AdderController : ApiController
    {
        readonly AdderService adderService;

        public AdderController(AdderService adderService)
        {
            this.adderService = adderService;
        }

        [HttpGet]
        public int Add(int firstNumber, int secondNumber)
        {
            return adderService.Add(firstNumber, secondNumber);    
        }
    }
}
