﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using BDDDemo.Services;

namespace BDDDemo.Controllers
{
    public class CounterController : System.Web.Http.ApiController
    {
        private CounterService counterService;

        public CounterController(CounterService counterService)
        {
            this.counterService = counterService;
        }

        [HttpGet]
        public int Count(){
            return counterService.Increment();
        }
    }
}
