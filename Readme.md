# BDD Demo

## Introduction
A demo application for **Behaviour Driven Development (BDD)** in C# using Cucumber

## Platform
The demo has been tested on Mac OS X 10.10+ and Windows 10.

## Installation

#### Install Visual Studio
Download Visual Studio from [https://www.visualstudio.com/](https://www.visualstudio.com/)

#### Clone Repository
Clone this repository using the command

`git clone https://bitbucket.org/xeric/bdddemo.net.git`

#### Add project to Visual Studio
Open Visual Studio and add the repository folder as a new solution. The solution should have two projects

1. **BDDDemo** - the application
2. **BDDDemo.Tests** - tests. 

#### Configuration

##### Windows
The server command in ```testserver.bat``` needs to be set to the test project in the solutions folder.

##### Mac
If you are using OS X, you need to setup the server path in ```testserver.sh``` present in the BDDDemo.Tests folder under the solution folder. 
To run the xsp server (web server) you need to find the path for mono executable and the xsp server.  Here is a command to find the command to run the server. The command should be run after the application has been started. (See **Run application** section to know how to starting the web server)

```
ps -ef `lsof -i:8080 | grep LISTEN | cut -f2 -d' '` | grep mono | sed -e 's/.*\(\/Library.*\)/\1/g' | sed -e 's/8080/9000 --applications=\/:./g'
```

## Usage

#### Run application

1. Right Click on the BDDDemo Project under the solution (in the left column window) and click **"Set As Startup Project"**
2. Click the run/play button at the top to run the project.
3. Point your browser to http://localhost:8080/

#### Run tests

1. Right Click on the BDDDemo.Tests Project under the solution (in the left column window) and click **"Set As Startup Project"**
2. Click the run/play button at the top to run the project.



