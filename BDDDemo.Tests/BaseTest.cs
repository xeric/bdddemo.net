﻿using System;
using BDDDemo.App_Start;
using Microsoft.Practices.Unity;
using TechTalk.SpecFlow;

namespace BDDDemo.Tests
{
    public class BaseTest
    {
        protected readonly IUnityContainer container;

        public BaseTest()
        {
            container = UnityConfig.GetConfiguredContainer();
        }

    }
}
