﻿using System;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace BDDDemo.Tests
{
    [Binding]
    public class DemoSteps
    {
        readonly DemoPage demoPage;
        private int count;

        public DemoSteps(DemoPage demoPage)
        {
            this.demoPage = demoPage;
        }

        [Given(@"^I am on the demo page$")]
        public void IAmOnDemoPage()
        {
            demoPage.Go();
        }


        [When(@"^I add the numbers (-?\d+) and (-?\d+)$")]
        public void IAddTwoNumbers(int firstNumber, int secondNumber)
        {
            demoPage.AddNumbers(firstNumber, secondNumber);
        }


        [Then(@"^the result is (-?\d+)$")]
        public void TheResultIs(int result)
        {
            demoPage.ExpectedResult(result);
        }

        [When(@"^the counter is called$")]
        public void TheCounterIsCalled()
        {
            count = demoPage.GetCount();
            demoPage.ClickIncrement();
        }


        [Then(@"^the count increases$")]
        public void TheCountIncreases()
        {
            demoPage.ExpectedCount(count + 1);
        }

    }
}
