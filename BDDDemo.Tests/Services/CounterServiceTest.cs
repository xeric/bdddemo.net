﻿using NUnit.Framework;
using System;
using BDDDemo.Services;

namespace BDDDemo.Tests.Services
{
    [TestFixture()]
    public class CounterServiceTest
    {
        [Test()]
        public void CounterTest()
        {
            CounterService service = new CounterService();
            Assert.AreEqual(0, service.Increment());
			Assert.AreEqual(1, service.Increment());
			Assert.AreEqual(2, service.Increment());
			Assert.AreEqual(3, service.Increment());
			Assert.AreEqual(4, service.Increment());
			Assert.AreEqual(5, service.Increment());
			Assert.AreEqual(6, service.Increment());
			Assert.AreEqual(7, service.Increment());
			Assert.AreEqual(8, service.Increment());
			Assert.AreEqual(9, service.Increment());
        }
    }
}
