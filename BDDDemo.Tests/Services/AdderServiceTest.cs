﻿using NUnit.Framework;
using System;
using BDDDemo.Services;

namespace BDDDemo.Tests.Services
{
    [TestFixture()]
    public class AdderServiceTest
    {
        AdderService adderService = new AdderService();

        [Test()]
        public void testAddition()
        {
            Assert.AreEqual(1, adderService.Add(1, 0));
            Assert.AreEqual(2, adderService.Add(1, 1));
            Assert.AreEqual(10, adderService.Add(5, 5));
            Assert.AreEqual(100, adderService.Add(75, 25));
        }

        [Test()]
        public void testAddWithNegativeNumbers()
        {
            Assert.AreEqual(-1, adderService.Add(-1, 0));
            Assert.AreEqual(-2, adderService.Add(-1, -1));
            Assert.AreEqual(0, adderService.Add(1, -1));
            Assert.AreEqual(-5, adderService.Add(5, -10));
            Assert.AreEqual(-50, adderService.Add(-75, 25));

        }
    }
}
