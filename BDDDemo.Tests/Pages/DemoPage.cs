﻿﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace BDDDemo.Tests
{
	public class DemoPage
	{
		private readonly IWebDriver driver;

		public DemoPage (IWebDriver driver)
		{
			this.driver = driver;
			PageFactory.InitElements (driver, this);
		}

        private WebDriverWait Wait()
        {
            return new WebDriverWait(driver, TimeSpan.FromSeconds(5));
        }

		public void Go ()
		{
            driver.Navigate().GoToUrl("http://localhost:9000");
            Wait().Until(ExpectedConditions.ElementIsVisible(By.Id("adder-first-number")));
		}

		public void AddNumbers (int x, int y)
		{
			driver.FindElement (By.Id ("adder-first-number")).SendKeys (x.ToString ());
			driver.FindElement (By.Id ("adder-second-number")).SendKeys (y.ToString ());
			driver.FindElement (By.Id ("adder-button")).Click ();
		}

        public void ExpectedResult(int result)
        {
            Wait().Until(ExpectedConditions.TextToBePresentInElementLocated(
                By.Id("adder-result"), result.ToString()));
		}

		public int GetAdderResults ()
		{
			return int.Parse (driver.FindElement (By.Id ("adder-result")).Text);

		}

        public void ExpectedCount(int count)
        {
            Wait().Until(ExpectedConditions.TextToBePresentInElementLocated(
                By.Id("counter"), count.ToString()));
        }

		public int GetCount ()
		{
			return int.Parse (driver.FindElement (By.Id ("counter")).Text);
		}

		public void ClickIncrement ()
		{
			driver.FindElement (By.Id ("increment-button")).Click ();

		}
	}
}
