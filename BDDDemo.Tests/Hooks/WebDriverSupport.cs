﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using BoDi;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace BDDDemo.Tests
{
    [Binding]
    public class WebDriverSupport
    {
        private readonly IObjectContainer objectContainer;
        private IWebDriver webDriver;

        public WebDriverSupport(IObjectContainer objectContainer)
        {
            this.objectContainer = objectContainer;
        }

        [BeforeScenario]
        public void initializeWebDriver()
        {
            webDriver = new ChromeDriver();
            objectContainer.RegisterInstanceAs<IWebDriver>(webDriver);
        }

        [AfterScenario]
        public void quitWebDriver()
        {
            if(webDriver != null)
            {
				webDriver.Quit();
			}
        }
    }
}
