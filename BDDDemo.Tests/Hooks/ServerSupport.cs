﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace BDDDemo.Tests.Steps
{
    [Binding]
    public class ServerSupport
    {
        Process process;

        private String TestDirectory()
        {
            return Directory.GetParent(
                    Directory.GetParent(
                        TestContext.CurrentContext.TestDirectory).ToString())
                                         .ToString();
        }

        private String TestDirectoryScript(String serverPath)
        {
            return Path.Combine(TestDirectory(), serverPath);
        }

        private String ServerExecutable()
        {
            var server = Environment.GetEnvironmentVariable("SERVER_STARTUP");
            if (server != null)
            {
                return server;
            }
            else
            {
                return IsRunningOnMono() ? TestDirectoryScript("testserver.sh") :
                    TestDirectoryScript("testserver.bat");
            }
        }

		private ProcessStartInfo ProcessInfo(string cmd)
		{
            return new ProcessStartInfo(cmd)
            {
                WorkingDirectory = TestDirectory(),
                CreateNoWindow = true,
                UseShellExecute = false,
				WindowStyle = ProcessWindowStyle.Hidden
			};
		}

		[BeforeScenario]
        private void StartServer()
        {
            process = new Process();
            process.StartInfo = ProcessInfo(ServerExecutable());
            process.Start();
        }

        public static bool IsRunningOnMono()
        {
            return Type.GetType("Mono.Runtime") != null;
        }

        [AfterScenario]
        private void StopServer()
        {
            if (process != null && !process.HasExited)
            {
                try
                {
                    process.Kill();
                    process.WaitForExit();
                }
                catch (Exception e)
                {
                    Console.Write("Exception killing server: " + e.Message);
                }
            }
        }


    }
}
