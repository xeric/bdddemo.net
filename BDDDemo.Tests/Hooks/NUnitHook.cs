﻿using System;
using System.IO;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace BDDDemo.Tests
{
	[Binding]
	public class NUnitHook
	{
		[BeforeFeature("workingDirectoryFeature")]
		public static void ChangeWorkingDirectory ()
		{
			FeatureContext.Current.Add ("NUnitHook.OldPWD", Directory.GetCurrentDirectory ());
			Directory.SetCurrentDirectory (TestContext.CurrentContext.TestDirectory);
		}

		[AfterFeature ("workingDirectoryFeature")]
		public static void RestoreWorkingDirectory ()
		{
			Directory.SetCurrentDirectory (FeatureContext.Current.Get<string> ("HUnitHook.OldPWD"));
		}


	}
}
