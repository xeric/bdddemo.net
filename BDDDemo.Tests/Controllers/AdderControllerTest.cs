﻿using System;
using BDDDemo.Services;
using NUnit.Framework;
using BDDDemo.Controllers;

namespace BDDDemo.Tests.Controllers
{
    [TestFixture]
    public class AdderControllerTest : BaseTest
    {

        [Test]
        public void TestAdder()
        {
            AdderController adderController = getAdderController();

            Assert.AreEqual(2, adderController.Add(1, 1));
            Assert.AreEqual(3, adderController.Add(2, 1));
            Assert.AreEqual(1000, adderController.Add(999, 1));
        }

        private AdderController getAdderController()
        {
			return (AdderController)container.Resolve(
				typeof(AdderController), "AdderController");
        }
    }

}
