﻿using System;
using BDDDemo.Services;
using NUnit.Framework;
using BDDDemo.Controllers;

namespace BDDDemo.Tests.Controllers
{
    [TestFixture]
    public class CounterControllerTest : BaseTest
    {
        [Test]
        public void TestCounter()
        {
            CounterController counterController = getCounterService();

            Assert.AreEqual(0, counterController.Count());
            Assert.AreEqual(1, counterController.Count());
            Assert.AreEqual(2, counterController.Count());
        }

        private CounterController getCounterService()
        {
            return (CounterController)container.Resolve(
                typeof(CounterController), "CounterController");
        }
    }
}
